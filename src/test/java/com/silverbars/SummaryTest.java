package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SummaryTest {
    private Summary view;
    private List<OrderVO> buyOrders = new ArrayList<>();
    private List<OrderVO> sellOrders = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        view = new Summary(buyOrders, sellOrders);
    }

    @Test
    public void should_return_buy_orders() throws Exception {
        List<OrderVO> orders = view.buys();

        assertThat(orders, is(buyOrders));
    }

    @Test
    public void should_return_sell_orders() throws Exception {
        List<OrderVO> orders = view.sells();

        assertThat(orders, is(sellOrders));
    }

}