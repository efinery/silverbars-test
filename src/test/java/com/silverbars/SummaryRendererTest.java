package com.silverbars;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SummaryRendererTest {
    private SummaryRenderer renderer;
    @Mock
    private OrderRenderer mockOrderRenderer;

    @Before
    public void setUp() throws Exception {
        renderer = new SummaryRenderer(mockOrderRenderer);
    }

    @Test
    public void should_render_empty_view() throws Exception {
        Summary view = new Summary(emptyList(), emptyList());

        String text = renderer.render(view);

        assertThat(text, is("BUY\nSELL\n"));
    }

    @Test
    public void should_render_multiple_orders() throws Exception {
        OrderVO orderVO = new OrderVO(1, 100);

        List<OrderVO> orderVOS = new ArrayList<>();
        orderVOS.add(orderVO);
        orderVOS.add(orderVO);

        when(mockOrderRenderer.render(orderVO)).thenReturn("[order]");
        Summary view = new Summary(orderVOS, orderVOS);

        String text = renderer.render(view);

        assertThat(text, is("BUY\n[order]\n[order]\nSELL\n[order]\n[order]\n"));
    }

}