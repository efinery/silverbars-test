package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OrderRendererTest {
    private OrderRenderer renderer;

    @Before
    public void setUp() throws Exception {
        renderer = new OrderRenderer();
    }

    @Test
    public void should_render_order() throws Exception {
        OrderVO orderVO = new OrderVO(5000, 30000);

        String text = renderer.render(orderVO);

        assertThat(text, is("5.0 kg for £300.00"));
    }

    @Test
    public void should_render_order_with_fractional_grams() throws Exception {
        OrderVO orderVO = new OrderVO(5500, 30000);

        String text = renderer.render(orderVO);

        assertThat(text, is("5.5 kg for £300.00"));
    }

    @Test
    public void should_render_order_with_pence() throws Exception {
        OrderVO orderVO = new OrderVO(5000, 29999);

        String text = renderer.render(orderVO);

        assertThat(text, is("5.0 kg for £299.99"));
    }

}