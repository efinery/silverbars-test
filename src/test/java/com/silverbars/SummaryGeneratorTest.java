package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.silverbars.Order.aBuyOrder;
import static com.silverbars.Order.aSellOrder;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SummaryGeneratorTest {
    private SummaryGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new SummaryGenerator(new OrderMerger());
    }

    @Test
    public void should_merge_buy_orders_and_sort_by_price_ascending() throws Exception {
        List<Order> orders = new ArrayList<Order>() {{
            add(aSellOrder().forUser("user1").forQuantity(3500).atPrice(30600).build());
            add(aSellOrder().forUser("user2").forQuantity(1200).atPrice(31000).build());
            add(aSellOrder().forUser("user3").forQuantity(1500).atPrice(30700).build());
            add(aSellOrder().forUser("user4").forQuantity(2000).atPrice(30600).build());
        }};

        Summary summary = generator.generate(orders);

        List<OrderVO> sells = summary.sells();
        assertThat(sells.size(), is(3));

        assertThat(sells.get(0).quantity(), is(5500));
        assertThat(sells.get(0).price(), is(30600));

        assertThat(sells.get(1).quantity(), is(1500));
        assertThat(sells.get(1).price(), is(30700));

        assertThat(sells.get(2).quantity(), is(1200));
        assertThat(sells.get(2).price(), is(31000));
    }

    @Test
    public void should_merge_sell_orders_and_sort_by_price_descending() throws Exception {
        List<Order> orders = new ArrayList<Order>() {{
            add(aBuyOrder().forUser("user1").forQuantity(1000).atPrice(10000).build());
            add(aBuyOrder().forUser("user2").forQuantity(2000).atPrice(15000).build());
            add(aBuyOrder().forUser("user3").forQuantity(3000).atPrice(10000).build());
        }};

        Summary summary = generator.generate(orders);

        List<OrderVO> buys = summary.buys();
        assertThat(buys.size(), is(2));

        assertThat(buys.get(0).quantity(), is(2000));
        assertThat(buys.get(0).price(), is(15000));

        assertThat(buys.get(1).quantity(), is(4000));
        assertThat(buys.get(1).price(), is(10000));
    }

    @Test
    public void should_generate_both_buys_and_sells() throws Exception {
        List<Order> orders = new ArrayList() {{
            add(aBuyOrder().forUser("user1").forQuantity(1000).atPrice(10000).build());
            add(aSellOrder().forUser("user2").forQuantity(2000).atPrice(15000).build());
        }};

        Summary summary = generator.generate(orders);

        assertThat(summary.buys().size(), is(1));
        assertThat(summary.sells().size(), is(1));
    }

}