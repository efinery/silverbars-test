package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import static com.silverbars.Order.aBuyOrder;
import static com.silverbars.Order.aSellOrder;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RenderLiveOrderBoardTest {
    private LiveOrderBoard board;
    private SummaryRenderer renderer;

    @Before
    public void setUp() throws Exception {
        board = new LiveOrderBoard(new Orders(), new SummaryGenerator(new OrderMerger()));
        renderer = new SummaryRenderer(new OrderRenderer());
    }

    @Test
    public void should_render_summary_as_plain_text() throws Exception {
        board.register(aSellOrder().forUser("user1").forQuantity(1000).atPrice(1000).build());
        board.register(aSellOrder().forUser("user2").forQuantity(1500).atPrice(1000).build());
        board.register(aSellOrder().forUser("user3").forQuantity(2000).atPrice(2000).build());

        board.register(aBuyOrder().forUser("user4").forQuantity(5000).atPrice(2000).build());
        board.register(aBuyOrder().forUser("user5").forQuantity(3000).atPrice(500).build());
        board.register(aBuyOrder().forUser("user6").forQuantity(2000).atPrice(2000).build());

        Summary summary = board.summary();
        String output = renderer.render(summary);

        assertThat(output, is("BUY\n7.0 kg for £20.00\n3.0 kg for £5.00\nSELL\n2.5 kg for £10.00\n2.0 kg for £20.00\n"));
    }

}