package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.silverbars.Order.aBuyOrder;
import static com.silverbars.Order.aSellOrder;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LiveOrderBoardTest {
    private LiveOrderBoard board;

    @Before
    public void setUp() throws Exception {
        board = new LiveOrderBoard(new Orders(), new SummaryGenerator(new OrderMerger()));
    }

    @Test
    public void should_register_order() throws Exception {
        Order sell = aSellOrder().forUser("user1").forQuantity(3500).atPrice(30300).build();

        assertThat(board.summary().sells().size(), is(0));
        board.register(sell);
        assertThat(board.summary().sells().size(), is(1));
    }

    @Test
    public void should_cancel_order() throws Exception {
        Order sell = aSellOrder().forUser("user1").forQuantity(3500).atPrice(30300).build();
        int orderID = board.register(sell);

        board.cancel(orderID);

        assertThat(board.summary().sells().size(), is(0));
    }

    @Test
    public void summary() throws Exception {
        board.register(aSellOrder().forUser("user1").forQuantity(1000).atPrice(1000).build());
        board.register(aSellOrder().forUser("user2").forQuantity(1500).atPrice(1000).build());
        board.register(aSellOrder().forUser("user3").forQuantity(2000).atPrice(2000).build());

        board.register(aBuyOrder().forUser("user4").forQuantity(5000).atPrice(2000).build());
        board.register(aBuyOrder().forUser("user5").forQuantity(3000).atPrice(500).build());
        board.register(aBuyOrder().forUser("user6").forQuantity(2000).atPrice(2000).build());

        Summary summary = board.summary();

        List<OrderVO> sells = summary.sells();
        assertThat(sells.size(), is(2));

        assertThat(sells.get(0).price(), is(1000));
        assertThat(sells.get(0).quantity(), is(2500));

        assertThat(sells.get(1).price(), is(2000));
        assertThat(sells.get(1).quantity(), is(2000));

        List<OrderVO> buys = summary.buys();
        assertThat(buys.size(), is(2));

        assertThat(buys.get(0).price(), is(2000));
        assertThat(buys.get(0).quantity(), is(7000));

        assertThat(buys.get(1).price(), is(500));
        assertThat(buys.get(1).quantity(), is(3000));

    }

}