package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OrderMergerTest {
    private OrderMerger merger;

    @Before
    public void setUp() throws Exception {
        merger = new OrderMerger();
    }

    @Test
    public void should_merge_orders_with_same_price() throws Exception {
        int price = 1000;
        List<OrderVO> orders = new ArrayList<OrderVO>() {{
            add(new OrderVO(500, price));
            add(new OrderVO(800, price));
        }};

        List<OrderVO> merged = merger.merge(orders);

        assertThat(merged.size(), is(1));
        assertThat(merged.get(0).price(), is(price));
        assertThat(merged.get(0).quantity(), is(1300));
    }

    @Test
    public void should_not_merge_orders_with_different_prices() throws Exception {
        List<OrderVO> orders = new ArrayList<OrderVO>() {{
            add(new OrderVO(500, 1000));
            add(new OrderVO(800, 1001));
        }};

        List<OrderVO> merged = merger.merge(orders);

        assertThat(merged.size(), is(2));

        assertThat(merged.get(0).price(), is(1000));
        assertThat(merged.get(0).quantity(), is(500));

        assertThat(merged.get(1).price(), is(1001));
        assertThat(merged.get(1).quantity(), is(800));
    }

}