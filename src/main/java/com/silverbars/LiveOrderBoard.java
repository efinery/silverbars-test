package com.silverbars;

import java.util.List;

public class LiveOrderBoard {
    private final Orders orders;
    private final SummaryGenerator summaryGenerator;

    public LiveOrderBoard(Orders orders,
                          SummaryGenerator summaryGenerator) {
        this.orders = orders;
        this.summaryGenerator = summaryGenerator;
    }

    public int register(Order order) {
        int orderID = orders.persist(order);
        return orderID;
    }

    public void cancel(int orderID) {
        orders.remove(orderID);
    }

    public Summary summary() {
        List<Order> allOrders = orders.findAll();
        Summary summary = summaryGenerator.generate(allOrders);
        return summary;
    }

}
