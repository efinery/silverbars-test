package com.silverbars;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public class SummaryGenerator {
    private final Comparator<OrderVO> priceAscending = new PriceAscending();
    private final OrderMerger merger;

    public SummaryGenerator(OrderMerger merger) {
        this.merger = merger;
    }

    public Summary generate(List<Order> allOrders) {
        Function<Order, OrderVO> toVO = order -> new OrderVO(order.getQuantity(), order.getPrice());

        List<OrderVO> buyOrders = allOrders.stream().filter(order -> order.isBuy()).map(toVO).collect(toList());
        List<OrderVO> sellOrders = allOrders.stream().filter(order -> order.isSell()).map(toVO).collect(toList());

        List<OrderVO> buys = merger.merge(buyOrders).stream().sorted(priceAscending.reversed()).collect(toList());
        List<OrderVO> sells = merger.merge(sellOrders).stream().sorted(priceAscending).collect(toList());
        return new Summary(buys, sells);
    }

    private static class PriceAscending implements Comparator<OrderVO> {
        @Override
        public int compare(OrderVO o1, OrderVO o2) {
            return o1.price() - o2.price();
        }
    }
}
