package com.silverbars;

import java.math.BigDecimal;

public class OrderRenderer {

    public String render(OrderVO order) {
        StringBuilder sb = new StringBuilder();
        sb.append(toKilograms(order.quantity()));
        sb.append(" kg for £");
        sb.append(toMoney(order.price()));
        return sb.toString();
    }

    private double toKilograms(int quantityInGrams) {
        return quantityInGrams / 1000.0;
    }

    private BigDecimal toMoney(int priceInPence) {
        double priceAsDouble = priceInPence / 100.0;
        return BigDecimal.valueOf(priceAsDouble).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

}
