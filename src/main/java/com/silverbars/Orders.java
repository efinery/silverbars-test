package com.silverbars;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Orders {
    private final AtomicInteger primaryKey = new AtomicInteger();
    private final Map<Integer, Order> orders = new ConcurrentHashMap<>();

    public int persist(Order order) {
        int orderID = primaryKey.incrementAndGet();
        orders.put(orderID, order);
        return orderID;
    }

    public void remove(int orderID) {
        orders.remove(orderID);
    }

    public List<Order> findAll() {
        return new ArrayList<>(orders.values());
    }
}
