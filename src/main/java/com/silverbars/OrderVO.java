package com.silverbars;

public class OrderVO {
    private final int quantityInGrams;
    private final int priceInPence;

    public OrderVO(int quantityInGrams, int priceInPence) {
        this.quantityInGrams = quantityInGrams;
        this.priceInPence = priceInPence;
    }

    public int quantity() {
        return quantityInGrams;
    }

    public int price() {
        return priceInPence;
    }
}
