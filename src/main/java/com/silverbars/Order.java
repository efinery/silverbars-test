package com.silverbars;

public class Order {
    private final String user;
    private final int quantity;
    private final int price;
    private final OrderType type;

    public static Builder aBuyOrder() {
        return new Builder().ofTypeBuy();
    }

    public static Builder aSellOrder() {
        return new Builder().ofTypeSell();
    }

    private Order(String user, int quantityInGrams, int pricePerKG, OrderType type) {
        this.user = user;
        this.quantity = quantityInGrams;
        this.price = pricePerKG;
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }

    public boolean isBuy() {
        return type == OrderType.BUY;
    }

    public boolean isSell() {
        return type == OrderType.SELL;
    }

    public static class Builder {
        private String user;
        private int quantityInGrams;
        private int pricePerKG;
        private OrderType type;

        public Order build() {
            if (user == null) {
                throw new IllegalStateException("User is required!");
            }
            if (quantityInGrams <= 0) {
                throw new IllegalStateException("Quantity is required!");
            }
            if (pricePerKG <= 0) {
                throw new IllegalStateException("Price is required!");
            }
            return new Order(user, quantityInGrams, pricePerKG, type);
        }

        Builder ofTypeBuy() {
            type = OrderType.BUY;
            return this;
        }

        Builder ofTypeSell() {
            type = OrderType.SELL;
            return this;
        }

        public Builder forUser(String user) {
            this.user = user;
            return this;
        }

        public Builder forQuantity(int quantityInGrams) {
            this.quantityInGrams = quantityInGrams;
            return this;
        }

        public Builder atPrice(int priceInPence) {
            this.pricePerKG = priceInPence;
            return this;
        }
    }
}
