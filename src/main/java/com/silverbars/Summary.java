package com.silverbars;

import java.util.List;

public class Summary {
    private final List<OrderVO> buyOrders;
    private final List<OrderVO> sellOrders;

    public Summary(List<OrderVO> buyOrders, List<OrderVO> sellOrders) {
        this.buyOrders = buyOrders;
        this.sellOrders = sellOrders;
    }

    public List<OrderVO> buys() {
        return buyOrders;
    }

    public List<OrderVO> sells() {
        return sellOrders;
    }

}