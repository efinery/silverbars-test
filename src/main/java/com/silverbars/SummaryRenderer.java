package com.silverbars;

public class SummaryRenderer {
    private OrderRenderer orderRenderer;

    public SummaryRenderer(OrderRenderer orderRenderer) {
        this.orderRenderer = orderRenderer;
    }

    public String render(Summary summary) {
        StringBuilder sb = new StringBuilder();

        sb.append("BUY\n");
        summary.buys().forEach(order -> sb.append(orderRenderer.render(order)).append("\n"));

        sb.append("SELL\n");
        summary.sells().forEach(order -> sb.append(orderRenderer.render(order)).append("\n"));

        return sb.toString();
    }

}
