package com.silverbars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderMerger {

    public List<OrderVO> merge(List<OrderVO> orders) {
        HashMap<Integer, OrderVO> merged = new HashMap<>();

        for (OrderVO order : orders) {
            int price = order.price();

            if (merged.containsKey(price)) {
                OrderVO existing = merged.remove(price);
                int mergedQuantity = order.quantity() + existing.quantity();
                OrderVO mergedOrder = new OrderVO(mergedQuantity, price);
                merged.put(price, mergedOrder);
            } else {
                merged.put(price, order);
            }
        }
        return new ArrayList<>(merged.values());
    }
}
