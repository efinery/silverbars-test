
- Price and Quantity were modelled as ints for simplicity, in pence and grams respectively.
  Usually I would model price with a Money class which wraps a BigDecimal to avoid loss in calculations.
- I chose to add a fluent Builder to create Orders for ease of use, and to avoid exposing enums.
- I didn't want to return a String from the getSummary() method as it's too rigid. I chose to return a Summary object
  containing view-agnostic Order Value Objects which are easier to interrogate/assert on. Now we have the option to render the view
  as a String, JSON, XML, display in GUI etc. depending on requirements. I provided a test called
  RenderLiveOrderBoardTest where I generate the required String output from my LiveOrderBoard and render using SummaryRenderer.
- I originally wanted to model the Order primary key on the Order, but since it's not even shown in the provided Summary output,
  I chose to simply return it from the register() method.
- In the spec it only shows SELL orders. There is no example of how both BUY and SELL orders are shown together. I chose to return both
  buy and sell orders in a Summary object for flexibility of rendering. I also chose to render BUY orders followed by SELL orders.
